const mongoose = require("mongoose");

const historySchema = new mongoose.Schema({
  dateFrom: { type: String, required: true },
  dateTo: { type: String, required: true },
  procurement: { type: String, required: true },
  department: { type: String, default: "General" },
});

const History = mongoose.model("History", historySchema);

module.exports = History;

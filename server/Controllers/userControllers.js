const axios = require("axios");
require("dotenv").config();
const History = require("../models/History");

const API_KEY = "FZEZeNnaVgLCrDMq624NMtbRAqod5ewtP5Dde5Hf";
const BASE_URL = "https://api.sam.gov/prod/opportunities/v2/search";

exports.api = async (req, res) => {
  res.status(200).json({ message: "Server is working" });
};

const fetchDescription = async (url) => {
  try {
    const fullUrl = `${url}&api_key=${API_KEY}`;
    console.log(fullUrl);
    const response = await axios.get(fullUrl);
    if (response.data && response.data.description) {
      return response.data.description;
    } else {
      return "Not Provided";
    }
  } catch (error) {
    console.error("Error fetching description:", error);
    return "Got some error";
  }
};

exports.getData = async (req, res) => {
  const { dateFrom, dateTo, procurement, department } = req.body;

  // Create a new instance of History with the provided data
  const newHistoryEntry = new History({
    dateFrom,
    dateTo,
    procurement,
    department,
  });

  try {
    // Save the new History entry to the database
    await newHistoryEntry.save();

    const response = await axios.get(BASE_URL, {
      params: {
        limit: 6,
        api_key: API_KEY,
        postedFrom: dateFrom,
        postedTo: dateTo,
        ptype: procurement,
        deptname: department.toLowerCase(),
      },
    });

    // Fetch descriptions for all opportunities
    const opportunitiesData = await Promise.all(
      response.data.opportunitiesData.map(async (opportunity) => {
        const award = opportunity.award
          ? {
              amount: opportunity.award.amount,
              awardeeName: opportunity.award.awardee.name, // Fixed the structure here
            }
          : null;

        const officeAddress = opportunity.officeAddress
          ? {
              zipcode: opportunity.officeAddress.zipcode,
              city: opportunity.officeAddress.city,
              countryCode: opportunity.officeAddress.country,
              state: opportunity.officeAddress.state,
            }
          : null;

        const pointOfContact =
          opportunity.pointOfContact && opportunity.pointOfContact[0]
            ? {
                email: opportunity.pointOfContact[0].email,
                phone: opportunity.pointOfContact[0].phone,
                fax: opportunity.pointOfContact[0].fax,
              }
            : null;

        const description = await fetchDescription(opportunity.description);
        const descriptionLink = opportunity.description
          ? `${opportunity.description}&api_key=${API_KEY}`
          : null;

        return {
          noticeId: opportunity.noticeId,
          title: opportunity.title,
          fullParentPathName: opportunity.fullParentPathName,
          postedDate: opportunity.postedDate,
          type: opportunity.type,
          active: opportunity.active,
          description,
          descriptionLink: descriptionLink,
          award,
          realAward: opportunity.award,
          organizationType: opportunity.organizationType,
          officeAddress,
          pointOfContact,
          placeOfPerformance: opportunity.placeOfPerformance,
        };
      })
    );

    const transformedData = {
      totalRecord: response.data.totalRecords,
      limit: response.data.limit,
      opportunitiesData,
    };

    console.log("Data from the API: ", transformedData);
    res.status(200).json(transformedData);
  } catch (error) {
    console.error("Error fetching data from API:", error);
    res
      .status(500)
      .json({ message: "Failed to fetch data from API", error: error.message });
  }
};

/*
exports.getData = async (req, res) => {
  const { dateFrom, dateTo, procurement, department } = req.body;

  // Create a new instance of History with the provided data
  const newHistoryEntry = new History({
    dateFrom,
    dateTo,
    procurement,
    department,
  });

  try {
    // Save the new History entry to the database
    await newHistoryEntry.save();

    const response = await axios.get(BASE_URL, {
      params: {
        limit: 6,
        api_key: API_KEY,
        postedFrom: dateFrom,
        postedTo: dateTo,
        ptype: procurement,
        deptname: department.toLowerCase(),
      },
    });

    const response = {
      data: {
        totalRecords: 34,
        limit: 1,
        offset: 0,
        opportunitiesData: [
          {
            noticeId: "5b345bbb7127b91a3ad577b203fc6f68",
            title: "Historic Office Renovation ",
            solicitationNumber: " 47PF0018R0023 ",
            department: "GENERAL SERVICES ADMINISTRATION",
            subTier: "PUBLIC BUILDINGS SERVICE",
            office: "PBS R5",
            postedDate: "2018-05-04",
            type: "Award Notice",
            baseType: "Combined Synopsis/Solicitation",
            archiveType: "manual",
            archiveDate: null,
            typeOfSetAsideDescription: null,
            typeOfSetAside: null,
            responseDeadLine: null,
            naicsCode: "236220",
            classificationCode: "Z",
            active: "Yes",
            placeOfPerformance: {
              streetAddress: "517 E Wisconsin Ave",
              city: {
                code: "53000",
                name: "Milwaukee",
              },
              state: {
                code: "WI",
              },
              zip: "53202",
              country: {
                code: "USA",
              },
            },
            award: {
              date: "2018-05-04",
              number: "47PF0018C0066",
              amount: "800620",
              awardee: {
                name: "D.G. Beyer, Inc.",
                location: {
                  streetAddress: "3080 S Calhoun Rd.",
                  city: {
                    code: "56375",
                    name: "New Berlin",
                  },
                  state: {
                    code: "WI",
                  },
                  zip: "53151",
                  country: {
                    code: "USA",
                  },
                },
                ueiSAM: "025114695AST",
              },
            },
            pointOfContact: [
              {
                fax: null,
                type: "primary",
                email: "jesse.jones@gsa.gov",
                phone: "2174941263",
                title: "Contracting Officer ",
                fullName: "Jesse L. Jones",
              },
            ],
            description:
              "https://api.sam.gov/prod/opportunities/v1/noticedesc?noticeid=5b345bbb7127b91a3ad577b203fc6f68",
            organizationType: "OFFICE",
            officeAddress: {
              zipcode: "60604",
              city: "CHICAGO",
              countryCode: "USA",
              state: "IL",
            },
            additionalInfoLink: null,
            uiLink:
              "https://beta.sam.gov/opp/5b345bbb7127b91a3ad577b203fc6f68/view",
            links: [
              {
                rel: "self",
                href: "https://api.sam.gov/prod/opportunities/v1/search?noticeid=5b345bbb7127b91a3ad577b203fc6f68&limit=1",
                hreflang: null,
                media: null,
                title: null,
                type: null,
                deprecation: null,
              },
            ],
          },
          {
            noticeId: "e3d3e84a866d47b8980538f593e5d60d",
            title: "61--HOUSING,ELECTRICAL",
            solicitationNumber: "SPE4A524T169U",
            fullParentPathName:
              "DEPT OF DEFENSE.DEFENSE LOGISTICS AGENCY.DLA AVIATION.DLA AV RICHMOND.DLA AVIATION",
            fullParentPathCode: "097.97AS.DLA AVIATION.DLA AV RICHMOND.SPE4A5",
            postedDate: "2024-05-27",
            type: "Award Notice",
            baseType: "Award Notice",
            archiveType: "auto15",
            archiveDate: "2024-06-11",
            typeOfSetAsideDescription: null,
            typeOfSetAside: null,
            responseDeadLine: null,
            naicsCode: "335311",
            naicsCodes: [Array],
            classificationCode: "61",
            active: "Yes",
            award: {
              date: "2018-05-04",
              number: "47PF0018C0066",
              amount: "800620",
              awardee: {
                name: "D.G. Beyer, Inc.",
                location: {
                  streetAddress: "3080 S Calhoun Rd.",
                  city: {
                    code: "56375",
                    name: "New Berlin",
                  },
                  state: {
                    code: "WI",
                  },
                  zip: "53151",
                  country: {
                    code: "USA",
                  },
                },
                ueiSAM: "025114695AST",
              },
            },
            pointOfContact: [
              {
                fax: null,
                type: "primary",
                email: "jesse.jones@gsa.gov",
                phone: "2174941263",
                title: "Contracting Officer ",
                fullName: "Jesse L. Jones",
              },
            ],
            description:
              "https://api.sam.gov/prod/opportunities/v1/noticedesc?noticeid=e3d3e84a866d47b8980538f593e5d60d",
            organizationType: "OFFICE",
            officeAddress: {
              zipcode: "60604",
              city: "CHICAGO",
              countryCode: "USA",
              state: "IL",
            },
            placeOfPerformance: null,
            additionalInfoLink: null,
            uiLink: "https://sam.gov/opp/e3d3e84a866d47b8980538f593e5d60d/view",
            links: [Array],
            resourceLinks: null,
          },
          {
            noticeId: "e39c8b09d5624cf7881cfef2fce18d1e",
            title: "62--LIGHT,INDICATOR",
            solicitationNumber: "SPRPA124T1611",
            fullParentPathName:
              "DEPT OF DEFENSE.DEFENSE LOGISTICS AGENCY.DLA AVIATION.DLA AVIATION PHILADELPHIA.DLA AVIATION AT PHILADELPHIA, PA",
            fullParentPathCode:
              "097.97AS.DLA AVIATION.DLA AV PHILADELPHIA.SPRPA1",
            postedDate: "2024-05-27",
            type: "Award Notice",
            baseType: "Award Notice",
            archiveType: "auto15",
            archiveDate: "2024-06-11",
            typeOfSetAsideDescription: null,
            typeOfSetAside: null,
            responseDeadLine: null,
            naicsCode: "334419",
            naicsCodes: [Array],
            classificationCode: "62",
            active: "Yes",
            award: {
              date: "2018-05-04",
              number: "47PF0018C0066",
              amount: "800620",
              awardee: {
                name: "D.G. Beyer, Inc.",
                location: {
                  streetAddress: "3080 S Calhoun Rd.",
                  city: {
                    code: "56375",
                    name: "New Berlin",
                  },
                  state: {
                    code: "WI",
                  },
                  zip: "53151",
                  country: {
                    code: "USA",
                  },
                },
                ueiSAM: "025114695AST",
              },
            },
            pointOfContact: [
              {
                fax: null,
                type: "primary",
                email: "jesse.jones@gsa.gov",
                phone: "2174941263",
                title: "Contracting Officer ",
                fullName: "Jesse L. Jones",
              },
            ],
            description:
              "https://api.sam.gov/prod/opportunities/v1/noticedesc?noticeid=e39c8b09d5624cf7881cfef2fce18d1e",
            organizationType: "OFFICE",
            officeAddress: {
              zipcode: "60604",
              city: "CHICAGO",
              countryCode: "USA",
              state: "IL",
            },
            placeOfPerformance: {
              streetAddress: "517 E Wisconsin Ave",
              city: {
                code: "53000",
                name: "Milwaukee",
              },
              state: {
                code: "WI",
              },
              zip: "53202",
              country: {
                code: "USA",
              },
            },
            additionalInfoLink: null,
            uiLink: "https://sam.gov/opp/e39c8b09d5624cf7881cfef2fce18d1e/view",
            links: [Array],
            resourceLinks: null,
          },
        ],
        links: [
          {
            rel: "self",
            href: "https://api.sam.gov/prod/opportunities/v1/search?limit=1&postedFrom=01/01/2018&postedTo=05/10/2018&ptype=a&deptname=general",
            hreflang: null,
            media: null,
            title: null,
            type: null,
            deprecation: null,
          },
        ],
      },
    };

    const transformedData = {
      totalRecord: response.data.totalRecords,
      limit: response.data.limit,
      opportunitiesData: response.data.opportunitiesData.map((opportunity) => {
        const award = opportunity.award
          ? {
              amount: opportunity.award.amount,
              awardeeName: opportunity.award.awardee.name, // Fixed the structure here
            }
          : null;

        const officeAddress = opportunity.officeAddress
          ? {
              zipcode: opportunity.officeAddress.zipcode,
              city: opportunity.officeAddress.city,
              countryCode: opportunity.officeAddress.country,
              state: opportunity.officeAddress.state,
            }
          : null;

        const pointOfContact =
          opportunity.pointOfContact && opportunity.pointOfContact[0]
            ? {
                email: opportunity.pointOfContact[0].email,
                phone: opportunity.pointOfContact[0].phone,
                fax: opportunity.pointOfContact[0].fax,
              }
            : null;

        //const description = await fetchDescription(opportunity.description);

        return {
          noticeId: opportunity.noticeId,
          title: opportunity.title,
          fullParentPathName: opportunity.fullParentPathName,
          postedDate: opportunity.postedDate,
          type: opportunity.type,
          active: opportunity.active,
          description: "Got some error",
          descriptionLink: opportunity.description,
          award,
          realAward: opportunity.award,
          organizationType: opportunity.organizationType,
          officeAddress,
          pointOfContact,
          placeOfPerformance: opportunity.placeOfPerformance,
        };
      }),
    };

    console.log("Data form the API: ", transformedData);
    res.status(200).json(transformedData);
  } catch (error) {
    console.error("Error fetching data from API:", error);
    res
      .status(500)
      .json({ message: "Failed to fetch data from API", error: error.message });
  }
};
*/

exports.getHistory = async (req, res) => {
  try {
    // Retrieve all history entries from the database
    const historyEntries = await History.find();

    // Send the retrieved history entries as the response
    res.status(200).json(historyEntries);
  } catch (error) {
    console.error("Error fetching history data:", error);
    res
      .status(500)
      .json({ message: "Failed to fetch history data", error: error.message });
  }
};

exports.getSummary = async (req, res) => {
  const { value } = req.body;

  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${process.env.REACT_APP_OPENAI_KEY}`,
    },
    data: JSON.stringify({
      prompt: value + `\n\nTl;dr`,
      temperature: 0.1,
      max_tokens: Math.floor(value.length / 2),
      top_p: 1,
      frequency_penalty: 0,
      presence_penalty: 0.5,
      stop: ['"""'],
    }),
    url: "https://api.openai.com/v1/engines/text-davinci-003/completions",
  };

  try {
    const response = await axios(requestOptions);

    if (
      !response.data ||
      !response.data.choices ||
      !response.data.choices[0] ||
      !response.data.choices[0].text
    ) {
      return res
        .status(500)
        .json({ message: "Failed to get a valid response from OpenAI API" });
    }

    const summary = response.data.choices[0].text.trim();

    // Assuming you want to store the summary in a database, add your database save logic here

    return res.status(200).json({ summary });
  } catch (error) {
    console.error("Error:", error);
    return res
      .status(500)
      .json({ message: "Error generating summary", error: error.message });
  }
};

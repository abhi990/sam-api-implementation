const express = require("express");
const router = new express.Router();
const controllers = require("../Controllers/userControllers.js");

router.get("/", controllers.api);

router.post("/getData", controllers.getData);
router.get("/getHistory", controllers.getHistory);
router.post("/getSummary", controllers.getSummary);

module.exports = router;

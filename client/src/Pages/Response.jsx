import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useLocation } from "react-router-dom";

const Response = () => {
  const location = useLocation();
  const navigate = useNavigate();

  // Getting data
  const { data } = location.state || {};
  const [selectedOpportunity, setSelectedOpportunity] = useState(null);

  // Read more handler
  const handleReadMore = (opportunity) => {
    setSelectedOpportunity(opportunity);
  };

  // Popup closer
  const handleClosePopup = () => {
    setSelectedOpportunity(null);
  };

  // AI Summary click handler
  const handleSummaryClick = () => {
    // Implement the logic for summary click
    console.log("Summary button clicked");
  };

  return (
    <div className="mb-32">
      <div
        className="flex justify-center"
        style={{
          background:
            "radial-gradient(circle,rgba(0,0,0,1) 40%,rgba(252,70,107,1) 100%)",
          height: "250px",
        }}
      >
        <h1 className="text-3xl sm:text-5xl text-white pt-12">
          SAM.gov Get Opportunities Public API Implementation
        </h1>
      </div>
      <div className="px-4 sm:w-2/3 lg:w-5/6 mx-auto">
        <div className="rounded-lg shadow-lg bg-white -mt-24 py-10 md:py-12 px-4 md:px-6">
          <div className="flex justify-center mb-6">
            <h1 className="text-3xl sm:text-3xl text-black pt-5">
              Response For Your Request
            </h1>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
            {data.opportunitiesData.map((opportunity) => (
              <div
                key={opportunity.noticeId}
                className="bg-white shadow-lg rounded-lg p-4"
              >
                <h2 className="text-xl font-semibold mb-2">
                  {opportunity.title}
                </h2>
                <p className="text-gray-600 mb-2">
                  Posted Date: {opportunity.postedDate}
                </p>
                <p className="text-gray-600 mb-2">Type: {opportunity.type}</p>
                <p className="text-gray-600 mb-2">
                  Award Amount: ${opportunity.award?.amount}
                </p>
                <button
                  className={`px-4 py-2 rounded text-white ${
                    opportunity.active === "Yes" ? "bg-green-500" : "bg-red-500"
                  }`}
                >
                  {opportunity.active === "Yes" ? "Active" : "Inactive"}
                </button>
                <button
                  className="mt-4 mx-2 px-4 py-2 bg-pink-600 text-white rounded  hover:bg-pink-700 hover:shadow-lg focus:bg-pink-700"
                  onClick={() => handleReadMore(opportunity)}
                >
                  Read More
                </button>
              </div>
            ))}
          </div>
          <div className="flex justify-center mt-6">
            <button
              className="px-6 py-3 font-medium rounded shadow-md hover:shadow-lg focus:outline-none focus:ring-0 bg-gray-500 text-white"
              onClick={() => navigate("/")}
            >
              Go Back
            </button>
          </div>
        </div>
      </div>

      {selectedOpportunity && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
          <div className="bg-white rounded-lg p-8 w-full max-w-2xl relative">
            <button
              className="absolute top-2 right-2 text-gray-600 hover:text-gray-800"
              onClick={handleClosePopup}
            >
              &times;
            </button>
            <h2 className="text-2xl font-bold mb-4">
              {selectedOpportunity.title}
            </h2>
            <p className="mb-2">
              <strong>Posted Date:</strong> {selectedOpportunity.postedDate}
            </p>
            <p className="mb-2">
              <strong>Type:</strong> {selectedOpportunity.type}
            </p>
            <p className="mb-2">
              <strong>Award Amount:</strong> $
              {selectedOpportunity.award?.amount}
            </p>
            <p className="mb-2">
              <strong>Description:</strong>
              {selectedOpportunity.description === "Got some error" &&
              selectedOpportunity.descriptionLink
                ? "Not Provided Description"
                : selectedOpportunity.description || "Not Provided"}
            </p>
            <p className="mb-2">
              <strong>Organization Type:</strong>
              {selectedOpportunity.organizationType}
            </p>
            <p className="mb-2">
              <strong>Office Address:</strong>
              {`${selectedOpportunity.officeAddress.city}, ${selectedOpportunity.officeAddress.state}, ${selectedOpportunity.officeAddress.zipcode}`}
            </p>
            <p className="mb-2">
              <strong>Point of Contact:</strong>
              {selectedOpportunity.pointOfContact?.email}
            </p>
            <p className="mb-2">
              <strong>Place of Performance:</strong>
              {selectedOpportunity.placeOfPerformance ? (
                <>
                  {selectedOpportunity.placeOfPerformance.streetAddress ||
                    "Not Provided"}
                  ,
                  {selectedOpportunity.placeOfPerformance.city?.name ||
                    "Not Provided"}
                  ,
                  {selectedOpportunity.placeOfPerformance.state?.code ||
                    "Not Provided"}
                  ,
                  {selectedOpportunity.placeOfPerformance.zip || "Not Provided"}
                  ,
                  {selectedOpportunity.placeOfPerformance.country?.code ||
                    "Not Provided"}
                </>
              ) : (
                "Not Provided"
              )}
            </p>
            <p className="mb-2">
              <strong>
                Names Of All Organizations Notice is Associated With:
              </strong>
              {selectedOpportunity.fullParentPathName
                ? selectedOpportunity.fullParentPathName
                : "Not Provided"}
            </p>
            <p className="mb-2">
              <strong>Notice Id:</strong>
              {selectedOpportunity.noticeId}
            </p>
            <button
              className="mt-4 bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600"
              onClick={handleSummaryClick} // Add a function to handle summary click
            >
              AI Summary
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Response;

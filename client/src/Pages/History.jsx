import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { format } from "date-fns";

// APIs
import { getHistory, getData } from "../Services/Apis";

const History = () => {
  const navigate = useNavigate();
  
  // Fetching user history from database using getHistory API
  const [historyData, setHistoryData] = useState([]);
  useEffect(() => {
    const fetchHistory = async () => {
      try {
        const response = await getHistory();
        setHistoryData(response.data);
      } catch (error) {
        console.error("Error fetching history data:", error);
      }
    };

    fetchHistory();
  }, []);

  /* Resend Handling Starts Here */
  const [isLoading, setIsLoading] = useState(false);

  // Original procurementTypeMapping
  const procurementTypeMapping = {
    Justification: "u",
    "Pre solicitation": "p",
    "Award Notice": "a",
    "Sources Sought": "r",
    "Special Notice": "s",
    Solicitation: "o",
  };

  // Create reverse mapping
  const reverseProcurementTypeMapping = Object.entries(
    procurementTypeMapping
  ).reduce((acc, [key, value]) => {
    acc[value] = key;
    return acc;
  }, {});


  // Function to handle Resend Request by user
  const onResend = async (data) => {
    setIsLoading(true); // Set loading state to true when form is submitted
    const { dateFrom, dateTo, procurement, ...rest } = data;

    const formattedData = {
      ...rest,
      dateFrom: dateFrom ? format(new Date(dateFrom), "MM/dd/yyyy") : "",
      dateTo: dateTo ? format(new Date(dateTo), "MM/dd/yyyy") : "",
      procurement: procurementTypeMapping[procurement] || procurement,
    };

    try {
      const response = await getData(formattedData);
      console.log("Response from backend:", response);
      navigate("/response", { state: { data: response.data } });
    } catch (error) {
      console.error("Error sending data to backend:", error);
    } finally {
      setIsLoading(false); // Set loading state to false after request completes
    }
  };
  /* Resend Handling Ends Here */

  return (
    <div className="mb-32">
      <div
        className="flex justify-center"
        style={{
          background:
            "radial-gradient(circle,rgba(0,0,0,1) 40%,rgba(252,70,107,1) 100%)",
          height: "250px",
        }}
      >
        <h1 className="text-3xl sm:text-5xl text-white pt-12">
          SAM.gov Get Opportunities Public API Implementation
        </h1>
      </div>
      <div className="px-4 sm:w-2/3 lg:w-3/4 mx-auto">
        <div className="rounded-lg shadow-lg bg-white -mt-24 py-10 md:py-12 px-4 md:px-6">
          <div className="flex justify-center mb-6">
            <h1 className="text-3xl sm:text-3xl text-black pt-5">
              Your Browsing History
            </h1>
          </div>
          <div className="container mx-auto px-4">
            <div className="overflow-x-auto">
              <table className="min-w-full bg-white border border-gray-200">
                <thead>
                  <tr>
                    <th className="px-4 py-2 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                      Department
                    </th>
                    <th className="px-4 py-2 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                      Posted From
                    </th>
                    <th className="px-4 py-2 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                      Posted To
                    </th>
                    <th className="px-4 py-2 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                      Procurement Type
                    </th>
                    <th className="px-4 py-2 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                      Resend
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {historyData.map((entry) => (
                    <tr key={entry._id} className="border-b border-gray-200">
                      <td className="px-4 py-2 text-sm">{entry.department}</td>
                      <td className="px-4 py-2 text-sm">{entry.dateFrom}</td>
                      <td className="px-4 py-2 text-sm">{entry.dateTo}</td>
                      <td className="px-4 py-2 text-sm">
                        {reverseProcurementTypeMapping[entry.procurement]}
                      </td>
                      <td className="px-4 py-2 text-sm">
                        <button
                          className="px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-700"
                          onClick={() =>
                            onResend({
                              dateFrom: entry.dateFrom,
                              dateTo: entry.dateTo,
                              procurement:
                                reverseProcurementTypeMapping[
                                  entry.procurement
                                ],
                              department: entry.department,
                            })
                          }
                          disabled={isLoading}
                        >
                          {isLoading ? "Resending..." : "Resend"}
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            <div className="flex justify-center mt-6">
              <button
                className="px-6 py-3 font-medium rounded shadow-md hover:shadow-lg focus:outline-none focus:ring-0 bg-gray-500 text-white"
                onClick={() => navigate("/")}
              >
                Go Back
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default History;

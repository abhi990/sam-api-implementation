import { useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { format } from "date-fns";

// Custome Components
import FormElement from "../Components/FormElement";
import IconInfo from "../Components/IconInfo";
import IconHistory from "../Icons/IcondHistory";

// APIs
import { getData } from "../Services/Apis";

const Home = () => {
  const navigate = useNavigate();
  
  /* Form Handling Starts Here */
  // State to manage loading state
  const [isLoading, setIsLoading] = useState(false);

  // Procurement Input field Mapping
  const procurementTypeMapping = {
    "Justification": "u",
    "Pre solicitation": "p",
    "Award Notice": "a",
    "Sources Sought": "r",
    "Special Notice": "s",
    Solicitation: "o",
  };

  // Form Structure using useForm
  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    defaultValues: {
      department: "General",
      procurement: "",
      dateFrom: null,
      dateTo: null,
    },
  });

  // handling Form Submit operation
  const onSubmit = async (data) => {
    setIsLoading(true); // Set loading state to true when form is submitted
    const { dateFrom, dateTo, procurement, ...rest } = data;

    // Formating input date
    const formattedData = {
      ...rest,
      dateFrom: dateFrom ? format(dateFrom, "MM/dd/yyyy") : "",
      dateTo: dateTo ? format(dateTo, "MM/dd/yyyy") : "",
      procurement: procurementTypeMapping[procurement] || procurement,
    };

    try {
      const response = await getData(formattedData);
      console.log("Response from backend:", response);
      navigate("/response", { state: { data: response.data } });
      reset(); // Reset form fields after successful submission
    } catch (error) {
      console.error("Error sending data to backend:", error);
    } finally {
      setIsLoading(false); // Set loading state to false after request completes
    }
  };
  /* Form Handling Ends Here */

  // Navigating to history page
  const handleClick = () => {
    navigate("/history");
  };

  return (
    <div className="mb-32">
      <div
        className="flex justify-center"
        style={{
          background:
            "radial-gradient(circle,rgba(0,0,0,1) 40%,rgba(252,70,107,1) 100%)",
          height: "250px",
        }}
      >
        <h1 className="text-3xl sm:text-5xl text-white pt-12">
          SAM.gov Get Opportunities Public API Implementation
        </h1>
      </div>
      <div className="px-4 sm:w-2/3 lg:w-1/2 mx-auto">
        <div className="rounded-lg shadow-lg bg-white -mt-24 py-10 md:py-12 px-4 md:px-6">
          <div className="flex justify-center mb-6">
            <button
              onClick={handleClick}
              className="px-6 py-3 font-medium rounded shadow-md hover:shadow-lg focus:outline-none focus:ring-0"
            >
              <IconInfo text="Check History" icon={<IconHistory />} />
            </button>
          </div>
          <div>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Controller
                name="department"
                control={control}
                defaultValue="General"
                render={({ field }) => (
                  <FormElement
                    type="text"
                    label="Department"
                    fieldRef={field}
                    disabled={true}
                    hasError={false}
                  />
                )}
              />
              <Controller
                name="procurement"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <FormElement
                    type="select"
                    label="Procurement Type"
                    options={[
                      "Justification",
                      "Pre solicitation",
                      "Award Notice",
                      "Sources Sought",
                      "Special Notice",
                      "Solicitation",
                    ]}
                    fieldRef={field}
                    hasError={errors.procurement?.type === "required"}
                  />
                )}
              />
              <div className="grid grid-cols-1 sm:grid-cols-2 gap-6">
                <Controller
                  name="dateFrom"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <FormElement
                      type="date"
                      label="Posted From"
                      fieldRef={field}
                      hasError={errors.dateFrom?.type === "required"}
                    />
                  )}
                />
                <Controller
                  name="dateTo"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <FormElement
                      type="date"
                      label="Posted To"
                      fieldRef={field}
                      hasError={errors.dateTo?.type === "required"}
                    />
                  )}
                />
              </div>
              <button
                type="submit"
                className="w-full px-6 py-3 bg-pink-600 text-white font-medium uppercase rounded shadow-md hover:bg-pink-700 hover:shadow-lg focus:bg-pink-700 focus:outline-none focus:ring-0 active:bg-pink-800"
                disabled={isLoading} // Disable button when loading
              >
                {isLoading ? "Loading..." : "Send"}
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;

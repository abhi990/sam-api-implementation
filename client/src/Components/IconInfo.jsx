const IconInfo = ({ icon, text }) => {
  return (
    <div className="text-center mx-auto">
      {icon}
      <h6 className="font-medium">{text}</h6>
    </div>
  );
};

export default IconInfo;

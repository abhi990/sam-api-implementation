import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const FormElement = ({
  type,
  label,
  placeholder,
  fieldRef,
  hasError,
  options,
}) => {
  const classes =
    "form-control w-full px-3 py-1.5 text-gray-700 rounded border border-solid border-gray-300 focus:border-pink-600 focus:outline-none";

  return (
    <div className="form-group mb-6">
      <label className="block text-gray-700 text-sm font-bold mb-2">
        {label}
      </label>
      {type === "select" ? (
        <select className={classes} {...fieldRef}>
          <option value="" disabled>
            Select an option
          </option>
          {options.map((option, index) => (
            <option key={index} value={option}>
              {option}
            </option>
          ))}
        </select>
      ) : type === "date" ? (
        <DatePicker
          className={classes}
          selected={fieldRef.value}
          onChange={fieldRef.onChange}
          placeholderText={placeholder}
        />
      ) : (
        <input
          className={classes}
          type={type}
          placeholder={placeholder}
          {...fieldRef}
        />
      )}

      {hasError && (
        <p className="text-red-500 text-xs italic">{`${label} is required`}</p>
      )}
    </div>
  );
};

export default FormElement;

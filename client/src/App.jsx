import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Home from "./Pages/Home";
import Response from "./Pages/Response";
import History from "./Pages/History";

const App = () => {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/response" element={<Response />} />
          <Route path="/history" element={<History />} />
        </Routes>
      </Router>
    </>
  );
};

export default App;

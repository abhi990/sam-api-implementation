import { commonrequest } from "./ApiCall";
import { BACKEND_URL } from "./Helper";

export const getData = async (data) => {
  return await commonrequest("POST", `${BACKEND_URL}/getData`, data);
};

export const getHistory = async (data) => {
  return await commonrequest("GET", `${BACKEND_URL}/getHistory`, data);
};
